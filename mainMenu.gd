
extends Control

var score_text

func _ready():
	score_text = get_node("score")
	var f = File.new()
	var high_score = 0
	if (f.open("user://highscore", File.READ) == OK):
		high_score = f.get_var()
		var minutes = high_score / 60
		var seconds = high_score % 60
		var str_high_score = "%02d : %02d" % [minutes, seconds]
		score_text.set_text(str_high_score)


func _on_btnStart_pressed():
	get_tree().change_scene("res://game.tscn")
