
extends Node2D

export var GAME_SPEED = 140.0

var platform1 = load("res://platform1.tscn")
var platform2 = load("res://platform2.tscn")
var platform3 = load("res://platform3.tscn")
var platform4 = load("res://platform4.tscn")
var platform5 = load("res://platform5.tscn")
var platform6 = load("res://platform6.tscn")
var platform7 = load("res://platform7.tscn")
var platform8 = load("res://platform8.tscn")

var endPlatform

var TILE_SIZE = 64

func _ready():
	
	endPlatform = _get_platform_endPos(get_child(2).get_node("sprite"))

func _process(delta):
	var count = 0
	for platform in get_children():
		var pos = platform.get_pos()
		var sprite = platform.get_node("sprite")
		var spriteSize = sprite.get_item_rect().size
		
		platform.set_pos(Vector2(pos.x - GAME_SPEED * delta, pos.y))
		
		if pos.x < 0 - spriteSize.x/2:
			platform.queue_free()
		
		endPlatform = _get_platform_endPos(sprite)
		count += 1
	
	if endPlatform > get_viewport_rect().size.y + TILE_SIZE and count < 10:
		_newPlatform()


func _newPlatform():
	var gapMultiplier = randi() % 3
	var platform = _get_platform()
	add_child(platform, true)
	platform.set_pos(Vector2(endPlatform + (TILE_SIZE * gapMultiplier) + (platform.get_node("sprite").get_item_rect().size.x / 2),420))

func _get_platform_endPos(sprite):
	return int(sprite.get_global_pos().x + (sprite.get_item_rect().size.x /2))
	
func _get_platform():
	var num = randi()% 7 + 1
	
	if num == 8:
		return platform8.instance()
	elif num == 7:
		return platform7.instance()
	elif num == 6:
		return platform6.instance()
	elif num == 5:
		return platform5.instance()
	elif num == 4:
		return platform4.instance()
	elif num == 3:
		return platform3.instance()
	elif num == 2:
		return platform2.instance()
	else:
		return platform1.instance()