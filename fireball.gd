
extends Area2D

const SPEED = 500

func _ready():
	set_process(true)

func _process(delta):
	translate(Vector2(delta*SPEED, 0))

func _on_fireball_area_enter( area ):
	if area in get_tree().get_nodes_in_group("enemy"):
		area._hit()
		queue_free()
	

func _on_VisibilityNotifier2D_exit_viewport( viewport ):
	queue_free()

func _on_fireball_body_enter( body ):
	
	if body in get_tree().get_nodes_in_group("enemy"):
		body._hit()
		queue_free()
