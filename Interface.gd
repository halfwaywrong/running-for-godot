
extends CanvasLayer

var player
var score_points
var game_over
var high_score
var start
var btnBlue
var btnRed
var up
var space
var heart = load("res://heart.tscn")
var hearts = []

func _ready():
	player = get_node("../player")
	score_points = get_node("score_points")
	game_over = get_node("game_over")
	high_score = get_node("high_score")
	start = get_node("start")
	btnBlue = get_node("btnBlue")
	btnRed = get_node("btnRed")
	up = get_node("up")
	space = get_node("space")
	
	_add_hearts()
	
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("jump"):
		player._jump()
	
	if event.is_action_pressed("shoot"):
		player._shoot()
	

func _on_btnBlue_pressed():
	player._jump()

func _on_btnRed_pressed():
	player._shoot()
	
func _add_hearts():
	var xPos = 30
	for count in range(0, player.health):
		var newHeart = heart.instance()
		newHeart.set_pos(Vector2(xPos, 50))
		hearts.insert(count, newHeart)
		add_child(hearts[count], false)
		xPos += newHeart.get_item_rect().size.x + 5

func _remove_heart(health):
	hearts[health].queue_free()

func _game_over():
	game_over.show()
	start.show()
	btnBlue.hide()
	btnRed.hide()
	up.hide()
	space.hide()

func _high_score():
	high_score.show()

func _on_start_pressed():
	get_tree().reload_current_scene()
