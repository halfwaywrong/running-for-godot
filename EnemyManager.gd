
extends Node2D

var fly = load("res://fly.tscn")
var count

var FLY_SPAWN_RATE = 5

func _ready():
	count = 0

func _process(delta):
	count += delta
	if count > FLY_SPAWN_RATE:
		_spawnFly()
		count = 0


func _spawnFly():
	var newFly = fly.instance()
	add_child(newFly, true)
	_set_Enemy_Pos(newFly)
	
func _set_Enemy_Pos(enemy):
	var viewportSize = get_viewport_rect().size
	var sprite = enemy.get_node("sprite")
	var spriteSize = enemy.get_item_rect().size
	enemy.set_pos(Vector2(viewportSize.x + spriteSize.x, rand_range(0, viewportSize.y)))