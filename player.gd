
extends RigidBody2D

var anim

var jumping = false;
var onGround = false;
var hurt = false;
var idle = false;
var health = 3

var JUMP_VELOCITY = 600

var fireball = preload("res://fireball.tscn")
var interface
var game

func _ready():
	
	game = get_node("/root/Game")
	interface = get_node("/root/Game/Interface")
	anim = get_node("anim")
	anim.play("run")
	
	set_process(true)

func _process(delta):
	var collidingBodies = get_colliding_bodies()
	for body in collidingBodies:
		if body.is_in_group("enemy") and !hurt:
			health -= 1
			hurt = true
			interface._remove_heart(health)
			anim.play("hurt")
	
	if health <= 0:
		_die()

func _integrate_forces(state):
	var lv = state.get_linear_velocity()
	
	if lv.y == 0:
		onGround = true
	else:
		onGround = false
		
	if jumping:
		lv.y = -JUMP_VELOCITY
		onGround = false
		jumping = false
	
	if onGround and anim.get_current_animation() != "run" and !hurt:
		anim.play("run")
	
	state.set_linear_velocity(lv)

func _jump():
	if !jumping and onGround:
		jumping = true
		if !hurt:
			anim.play("jump")

func _shoot():
	var fb = fireball.instance()
	var pos = get_pos() + get_node("fireball_emitter").get_pos()
	
	fb.set_pos(pos)
	get_parent().add_child(fb)

func _on_hurt_finish():
	hurt = false

func _die():
	game._game_over()	
	queue_free()

func _on_VisibilityNotifier2D_exit_screen():
	game._game_over()
	queue_free()
