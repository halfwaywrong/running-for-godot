
extends Node2D

var player
var interface
var platformManager
var enemyManager
var screen_size
var sec_elapsed = 0

var time_start = 0
var time_now = 0
var elapsed

func _ready():
	
	randomize()
	
	player = get_node("player")
	interface = get_node("Interface")
	platformManager = get_node("PlatformManager")
	enemyManager = get_node("EnemyManager")
	screen_size = get_viewport_rect().size
	time_start = OS.get_unix_time()
	set_process(true)

func _process(delta):
	_updateScore()
	platformManager._process(delta)
	enemyManager._process(delta)

func _game_over():
	set_process(false)
	interface._game_over()
	
	var high_score = 0
	var f = File.new()
	if (f.open("user://highscore", File.READ) == OK):
		high_score = f.get_var()
		if high_score == null:
			high_score = 0
	
	if elapsed > int(high_score):
		f.open("user://highscore", File.WRITE)
		f.store_var(elapsed)
		interface._high_score()

func _updateScore():
	time_now = OS.get_unix_time()
	elapsed = time_now - time_start
	var minutes = elapsed / 60
	var seconds = elapsed % 60
	var str_elapsed = "%02d : %02d" % [minutes, seconds]
	interface.score_points.set_text(str_elapsed)