Running For Godot

By Michael Henderson - https://halfwaywronggames.tumblr.com

My entry in the Godot Game Jam January 2017.
Apologies in advance that there's no folder structure - this game got way bigger than I originally thought it was going to be!


All code and assets are Public Domain CC0.
These are things I didn't make and who should take the credit for them:
Level and character artwork:
Kenney - http://kenney.nl/assets
GUI artwork:
CTatz - http://opengameart.org/content/free-ui-asset-pack-1
Music:
Écrivain - http://opengameart.org/content/meadow-thoughts