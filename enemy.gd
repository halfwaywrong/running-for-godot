
extends RigidBody2D

const SPEED = 10
const HEALTH = 1

var anim
var sprite
var curHealth = 1
var dying = false

func _ready():
	sprite = get_node("sprite")
	anim = get_node("anim")
	anim.play("fly")
	
	set_process(true)

func _process(delta):
	var pos = get_pos()
	set_pos(Vector2(pos.x - SPEED * delta, pos.y))
	
	if pos.x < 0 - sprite.get_item_rect().size.x/2:
		queue_free()

func _hit():
	curHealth -= 1
	if curHealth <= 0 && !dying:
		dying = true
		anim.play("hurt")

func _die():
	queue_free()
	