
extends Sprite

var ROT_SPEED = 10

func _ready():
	set_process(true)


func _process(delta):
	set_rot(get_rot() - ROT_SPEED * delta)